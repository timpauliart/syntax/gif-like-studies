package n01.utility;

import processing.core.PApplet;

public class Utility {
	public static float unnorm(float value, float min, float max) {
		return PApplet.map(value, 0, 1, min, max);
	}

	public static int unnorm(float value, int min, int max) {
		return (int) PApplet.map(value, 0, 1, (float) min, (float) max);
	}

	public static int colorToInt(int Red, int Green, int Blue) {
		Red = (Red << 16) & 0x00FF0000; // Shift red 16-bits and mask out other stuff
		Green = (Green << 8) & 0x0000FF00; // Shift Green 8-bits and mask out other stuff
		Blue = Blue & 0x000000FF; // Mask out anything not blue.

		return 0xFF000000 | Red | Green | Blue; // 0xFF000000 for 100% Alpha. Bitwise OR everything together.
	}
}