package n01.core;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import n01.utility.Utility;
import processing.core.PApplet;
import processing.core.PVector;

public class VaporWave extends ArtObject {
// CONSTANTS
	protected final int MAX_DUR = 20 * 30; // in frames
	protected final int MIN_DUR = 5 * 30; // in frames
	protected final int MAX_COUNT = 30;
	protected final int MIN_COUNT = 1;

	protected final float MAX_SIZE = 1080;
	protected final float MIN_SIZE = 270;
	protected final float MAX_SLOW = 90;
	protected final float MIN_SLOW = 9;
	protected final int MAX_STEP = 270;
	protected final int MIN_STEP = 1;

	protected final float MAX_ROT = 0.01f;
	protected final float MIN_ROT = -0.01f;
	protected final float MAX_DIS = 1920;
	protected final float MIN_DIS = 270;

	protected float slow;
	protected int step;
	
	protected List<PVector> sizes;
// back
	protected int backgroundColor;
// material
	protected int ambient;
	protected int emissive;
	protected int specular;

	public VaporWave() {
		this.ikeda = new Ikeda(1, 1, 1, 0.7f);
		this.framesLeft = 0;
		this.t = 0;
		this.isOver();
	}

	public void frameGoesOn() {
		this.framesLeft -= 1;
		this.t += 1;
	}

	public boolean isOver() {
		if (this.framesLeft <= 0) {
			PVector i1 = this.ikeda.next();
			this.framesLeft = Utility.unnorm(i1.x, this.MIN_DUR, this.MAX_DUR);
			this.count = Utility.unnorm(i1.y, this.MIN_COUNT, this.MAX_COUNT);
			this.backgroundColor = this.getColor(i1.z);
			PVector i2 = this.ikeda.next();
			float rotationX = Utility.unnorm(i2.x, this.MIN_ROT, this.MAX_ROT);
			float rotationY = Utility.unnorm(i2.y, this.MIN_ROT, this.MAX_ROT);
			this.rotation = new PVector(rotationX, rotationY);
			this.distance = Utility.unnorm(i2.z, this.MIN_DIS, this.MAX_DIS);
			PVector i3 = this.ikeda.next();
			this.ambient = this.getColor(i3.x);
			this.emissive = this.getColor(i3.y);
			this.specular = this.getColor(i3.z);
			PVector i4 = this.ikeda.next();
			this.size = Utility.unnorm(i4.x, this.MIN_SIZE, this.MAX_SIZE);
			this.slow = Utility.unnorm(i4.y, this.MIN_SLOW, this.MAX_SLOW);
			this.step = Utility.unnorm(i4.z, this.MIN_STEP, this.MAX_STEP);
			this.sizes = IntStream.range(0, 60).mapToObj(x -> this.makeSize(x)).collect(Collectors.toList()); // TODO dirty hack 60
			return true;
		} else {
			return false;
		}
	}

	public PVector getCoordinate(int future) {
		float x = PApplet.sin((this.t + (future * this.step)) / this.slow) * this.size;
		float y = PApplet.sin((this.t + (future * this.step)) / this.slow) * this.size;
		float z = PApplet.sin((this.t + (future * this.step)) / this.slow) * this.size;
		return new PVector(x, y, z);
	}

	private PVector makeSize(int future) {
		float x = (PApplet.sin((this.t + (future * this.step)) / (this.slow + 0.1f)) * this.size);
		float y = (PApplet.sin((this.t + (future * this.step)) / (this.slow + 0.2f)) * this.size);
		float z = (PApplet.sin((this.t + (future * this.step)) / (this.slow + 0.3f)) * this.size);
		return new PVector(x, y, z);
	}
	
	public PVector getSize(int future) {
		return this.sizes.get(future);
	}
	
	public int getCount() {
		return this.count;
	}

	public int getBackgroundColor() {
		return this.backgroundColor;
	}

	public int getAmbient() {
		return this.ambient;
	}

	public int getEmissive() {
		return this.emissive;
	}

	public int getSpecular() {
		return this.specular;
	}

	public float getShininess() {
		return 0;
	}

	public PVector getRotation() {
		return this.rotation;
	}

	public float getDistance() {
		return this.distance;
	}
	
	public float getSlowness() {
		return this.slow;
	}


	public int getColor(float value) {
		int[] colors = { 0xFF000000, 0xFFff0000, 0xFF00ff00, 0xFF0000ff, 0xFFffff00, 0xFFff00ff, 0xFF00ffff, 0xFFff8000,
				0xFFff0080, 0xFF80ff00, 0xFF00ff80, 0xFF8000ff, 0xFF0080ff, 0xFFffffff };
		int index = Utility.unnorm(value, 0, colors.length - 1);
		int result = colors[index];
		return result;
	}

	public String toString() {
		String result = "sin((" + this.t + "+(future*" + this.step + "))/" + this.slow + ")*" + this.size;
		return result;
	}
}
