package n01.core;

import processing.core.PVector;
import processing.core.PApplet;

public class Ikeda {
private float x;
private float y;
private float z;
private float u;

public Ikeda(float x, float y, float z, float u){
        this.x = x;
        this.y = y;
        this.z = z;
        this.u = u;
}

public PVector next(){
        float x = 1 + this.u * (this.x * PApplet.cos(this.z) - this.y * PApplet.sin(this.z));
        float y = this.u * (this.x * PApplet.sin(this.z) + this.y * PApplet.cos(this.z));
        float z = 0.4f - (6.0f / (1 + this.x * this.x + this.y * this.y));
        this.x = x;
        this.y = y;
        this.z = z;
        x = PApplet.abs(x / 2.0f);
        y = PApplet.abs(y / 2.0f);
        z = PApplet.abs(z / 6.0f);
        return new PVector(x, y, z);
}
}
