package n01.core;

import processing.core.PVector;

abstract class ArtObject implements ArtMethods {
	// time
	protected int t;
	protected int framesLeft;
	// random
	protected Ikeda ikeda;
	//
	protected int count;
	protected float size;

	// cam
	protected PVector rotation;
	protected float distance;

}
