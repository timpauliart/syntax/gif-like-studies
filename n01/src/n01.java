import beads.AudioContext;
import ch.bildspur.postfx.builder.PostFX;
import guru.ttslib.TTS;
import n01.audio.BlackSpeak;
import n01.audio.PadChord;
import n01.audio.VaporSpeak;
import n01.core.ArtMethods;
import n01.core.BlackMetal;
import n01.core.Ikeda;
import n01.core.VaporWave;
import peasy.PeasyCam;
import processing.core.PApplet;
import processing.core.PVector;

/**
 * @author inkeye
 *
 */
public class n01 extends PApplet {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main("n01");
	}
	
	// video
	PeasyCam cam;
	PostFX fx;
	TTS tts;

	// audio
	AudioContext ac;
	PadChord pad;
	VaporSpeak vSpeak;
	BlackSpeak bSpeak;
	// own
	VaporWave vapor;
	BlackMetal black;
	Ikeda learnIkeda;

	// logic
	boolean blackIsOn = false;
	PVector currentLearning = new PVector(0, 0, 0);

	public void settings() {
		smooth(8);
		size(1920, 1080, P3D);
	}

	public void setup() {
		frameRate(30);
		noStroke();
		// video
		fx = new PostFX(this);
		cam = new PeasyCam(this, 1080);
		cam.setActive(false);
		// audio
		System.setProperty("mbrola.base", "/usr/bin/");
		tts = new TTS("mbrola_us1");
		ac = new AudioContext();
		pad = new PadChord(ac, 50);
		ac.out.addInput(pad);
		bSpeak = new BlackSpeak(ac);
		ac.out.addInput(bSpeak);
		ac.start();
		// own
		vapor = new VaporWave();
		black = new BlackMetal();
		learnIkeda = new Ikeda(1, 1, 1, 0.9f);
		vaporSoundOn();

	}

	public void draw() {
		if (blackIsOn) {
			drawObject(black, vapor, currentLearning.y);
			if (black.isOver()) {
				blackIsOn = false;
				vaporSoundOn();
				blackSoundOff();
			}
			fx.render().bloom(0.1f, 10, 10).compose();
		} else {
			drawObject(vapor, black, currentLearning.x);
			if (vapor.isOver()) {
				blackIsOn = true;
				currentLearning = learnIkeda.next();
				blackSoundOn();
				vaporSoundOff();
			}
			fx.render().bloom(0.5f, 10, 10).compose();
		}
		 saveFrame();
	}

	void drawObject(ArtMethods object, ArtMethods enemy, float learning) {
		// light
		myLights(1.0f);
		// camera
		PVector rotation;
		if (learning < 0.45) {
			rotation = object.getRotation();
		} else {
			rotation = enemy.getRotation();
		}
		cam.rotateY(rotation.y);
		cam.rotateX(rotation.x);

		if (learning < 0.5) {
			cam.setDistance(object.getDistance(), 0);
		} else {
			cam.setDistance(enemy.getDistance(), 0);
		}
		
		int count;
		if (learning < 0.55) {
			count = object.getCount();
		} else {
			count = enemy.getCount();
		}
		
		// hard cut

		if (learning < 0.6) {
			ambient(object.getAmbient());
		} else {
			ambient(enemy.getAmbient());
		}

		if (learning < 0.65) {
			emissive(object.getEmissive());
		} else {
			emissive(enemy.getEmissive());
		}

		if (learning < 0.7) {
			specular(object.getSpecular());
		} else {
			specular(enemy.getSpecular());
		}

		if (learning < 0.75) {
			shininess(object.getShininess());
		} else {
			shininess(enemy.getShininess());
		}

		// back
		if (learning < 0.85) {
			background(object.getBackgroundColor());
		} else {
			background(enemy.getBackgroundColor());
		}

		for (int i = 0; i < count; ++i) {
			PVector c = object.getCoordinate(i);
			PVector size = object.getSize(i);
			// draw
			translate(c.x, c.y, c.z);
			box(size.x, size.y, size.z);
			translate(-c.x, -c.y, -c.z);
		}
		// time
		object.frameGoesOn();
	}

	void myLights(float f) {
		ambientLight(128 * f, 128 * f, 128 * f);
		directionalLight(92 * f, 92 * f, 92 * f, 0, 0, -1);
		directionalLight(48 * f, 48 * f, 48 * f, 0, -1, 0);
		directionalLight(0 * f, 0 * f, 0 * f, -1, 0, 0);
		directionalLight(92 * f, 92 * f, 92 * f, 0, 0, 1);
		directionalLight(48 * f, 48 * f, 48 * f, 0, 1, 0);
		directionalLight(0 * f, 0 * f, 0 * f, 1, 0, 0);
		lightFalloff(1, 1, 1);
		lightSpecular(128 * f, 128 * f, 128 * f);
	}

	void vaporSoundOn() {
		this.vSpeak = new VaporSpeak(this.tts, this.vapor);
		vSpeak.start();
		pad.nextRoot(random(0, 1));
		pad.fade(0.8f, 500);
	}

	void vaporSoundOff() {
		pad.fade(0, 500);
//		try {
//			this.speak.
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	void blackSoundOn() {
		this.bSpeak.fade(0.8f, 50);

	}

	void blackSoundOff() {
		this.bSpeak.fade(0, 50);
//		try {
//			this.speak.
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

}
